import fetch from './fetch'

export const MOONPAY_API_KEY = 'pk_test_nn2SG1yRBLhKLUtr5rB5hw86kYu1LTq';

export const fetchMoonpaySignature = async (walletAddress) => {
  const query = encodeURI(`?apiKey=${MOONPAY_API_KEY}&walletAddress=${walletAddress}&currencyCode=matic`);
  return fetch.get(`https://uazgzvkm0b.execute-api.eu-central-1.amazonaws.com/prod/sya/sign-moonpay`, { params: { query} }).then(r => r.data);
}