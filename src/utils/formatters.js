import React, {useEffect, useState} from 'react';
import {ChainId} from '@flooz/marketplace-sdk';

import { ReactComponent as AvxIcon } from '../assets/avx.svg';
import {floozMarketplace} from '../context/AuthProvider'
import LoadingDots from "../components/LoadingDots";
import {formatEther} from "@ethersproject/units";

export const FormatAvax = ({value}) => {
  const [usd, setUSD] = useState(null);
  useEffect(() => {
    (async () => {
      try {
        const newusd = await floozMarketplace.usdToCoin(formatEther(value), ChainId.AVALANCE_FUJI_TEST);
        setUSD(newusd);
      } catch (e) {
        console.log(e);
      }
    })();
  }, [value])

  return (
    <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
      <AvxIcon style={{marginRight: 10}} />
      <div>
        {value && formatEther(value)}
      </div>
      <div style={{marginRight: 10, marginLeft: 10}}>
        =
      </div>
      <div style={{ minWidth: 30}}>
        {
          usd !== null ? `$${usd.toFixed(2)}` : <LoadingDots />
        }
      </div>
    </div>
  );
}

export const FormatUSD = ({value}) => {
  const [avax, setAvax] = useState(null);
  useEffect(() => {
    (async () => {
      const newavax = await floozMarketplace.usdFromCoin(value, ChainId.AVALANCE_FUJI_TEST);
      setAvax(newavax);
    })();
  }, [value])

  return (
    <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
      <AvxIcon style={{marginRight: 10}} />
      <div>
        {avax ? parseFloat(value).toFixed(4) : <LoadingDots />}
      </div>
      <div style={{marginRight: 10, marginLeft: 10}}>
        =
      </div>
      <div style={{ minWidth: 30}}>
        {`$${value}`}
      </div>
    </div>
  );
}