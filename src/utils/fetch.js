import axios from 'axios';

const defaultOptions = {
    baseURL: 'http://kanyewest.com',
    headers: {
        'Content-Type': 'application/json',
    }
};
const Fetch = axios.create(defaultOptions);

export default Fetch;
