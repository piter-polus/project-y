import React, { useState } from 'react';

export const BottomSheetsContext = React.createContext({
    activeBottomSheet: null,
    activeBottomSheetData: {},
    open: () => {},
    close: () => {},
});

const BottomSheetsProvider = ({ children }) => {
    const [activeBottomSheet, setActiveBottomSheet] = useState(null);
    const [activeBottomSheetData, setActiveBottomSheetData] = useState({});

    const open = (id, data) => {
        setActiveBottomSheet(id);
        setActiveBottomSheetData(data || {});
    }

    const close = () => {
        setActiveBottomSheet(null);
        setActiveBottomSheetData({});
    }

    const contextValue = {
        open,
        close,
        activeBottomSheet,
        activeBottomSheetData,
    };

    return <BottomSheetsContext.Provider value={contextValue}>{children}</BottomSheetsContext.Provider>;
};

export default BottomSheetsProvider;
