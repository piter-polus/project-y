import React, {useEffect, useState,useContext} from 'react';
import {FloozProviders, ChainId, WETH, MINT_TOKEN, MINT_FEE, FloozMarketplace} from '@flooz/marketplace-sdk';
import {toast} from 'react-toastify';
import Web3 from "web3";
import Web3Modal from "web3modal";
import WalletConnectProvider from "@walletconnect/web3-provider";
import { WalletLink } from 'walletlink';
import useStateRef from 'react-usestateref';
import {parseEther} from "@ethersproject/units";

import {BottomSheetsContext} from "./BottomSheetsProvider";

export const CHAIN_ID = ChainId.AVALANCE_FUJI_TEST;
export const NFT_ADDRESS = MINT_TOKEN[CHAIN_ID];
export const NFT_MINT_FEE = MINT_FEE[CHAIN_ID];
export const AVAILABLE_NETWORKS = [CHAIN_ID];

let provider;
let web3;
export let floozMarketplace;

const initWeb3 = async (provider) => {
  const newweb3 = new Web3(provider);

  newweb3.eth.extend({
    methods: [
      {
        name: "chainId",
        call: "eth_chainId",
        outputFormatter: Web3.utils.hexToNumber
      }
    ]
  });

  web3 = newweb3;
}

const initFlooz = async () => {
  floozMarketplace = new FloozMarketplace(CHAIN_ID);
}

const web3Modal = new Web3Modal({
  cacheProvider: false,
  disableInjectedProvider: false,
  providerOptions: {
    walletconnect: {
      package: WalletConnectProvider,
      options: {
        rpc: FloozProviders
      },
    },
    "custom-walletlink": {
      display: {
        logo: 'coinbase-wallet.svg',
        name: 'WalletLink',
        description: 'Scan with WalletLink to connect',
      },
      options: {
        appName: 'DONDAVERSE',
        networkUrl: FloozProviders[CHAIN_ID].connection.url,
        chainId: CHAIN_ID,
      },
      package: WalletLink,
      connector: async (_, options) => {
        const { appName, networkUrl, chainId } = options
        const walletLink = new WalletLink({
          appName
        });
        const provider = walletLink.makeWeb3Provider(networkUrl, chainId);
        await provider.enable();
        return provider;
      },
    }
  },
});

export const AuthContext = React.createContext({});

const AuthProvider = ({children}) => {
  const [account, setAccount, accountRef] = useStateRef(null);
  const [network, setNetwork] = useState(null);
  const [accountNFTs, setAccountNFTs] = useState([]);
  const [openOrders, setOpenOrders] = useState([]);
  const [accountOrders, setAccountOrders] = useState([]);

  const {open, close} = useContext(BottomSheetsContext);

  const connect = async () => {
    try {
      await web3Modal.clearCachedProvider();
      provider = await web3Modal.connect();
      await initWeb3(provider);
      await initFlooz(provider);
      await fetchOpenOrders();
      provider.on("accountsChanged", async () => {
        await fetchAccount();
      });

      provider.on("chainChanged", async () => {
        await fetchAccount();
      });

      provider.on("connect", (info) => {
        console.log(info);
      });

      provider.on("disconnect", (error) => {
        console.log(error);
        setAccount(null);
      });

      toast("Account connected")
      return await fetchAccount();
    } catch (e) {
      toast('UNEXPECTED ERROR');
      console.log("Could not get a wallet connection", e);
    }
  }

  const disconnect = async () => {
    if (provider.close) {
      await provider.close();
      await web3Modal.clearCachedProvider();
      provider = null;
    }
    setAccount(null);
  }

  const fetchAccount = async () => {
    try {
      const accounts = await web3.eth.getAccounts();
      await setAccount(accounts[0]);
      await setNetwork(await web3.eth.getChainId());
      return accounts[0];
    } catch (e) {
      toast('UNEXPECTED ERROR');
      console.log(e);
    }
  }

  const fetchAccountNFTs = async ({invalidateCache} = {}) => {
    try {
      if (accountRef.current && floozMarketplace) {
        const userNfts = await floozMarketplace.getUserNFTs(accountRef.current, NFT_ADDRESS, invalidateCache);
        setAccountNFTs(userNfts.map(i => ({tokenId: parseInt(i)})));
      } else {
        console.log('empty');
        setAccountNFTs([]);
      }
    } catch (e) {
      toast('UNEXPECTED ERROR');
      console.log(e);
    }
  }

  const fetchOpenOrders = async () => {
    try {
      setOpenOrders(await floozMarketplace.getOpenOrders(NFT_ADDRESS, CHAIN_ID));
    } catch (e) {
      toast('UNEXPECTED ERROR');
      console.log(e);
    }
  }

  const fetchAccountOrders = async () => {
    try {
      if (accountRef.current && floozMarketplace) {
        const userOrders = await floozMarketplace.getOpenUserOrders(accountRef.current, NFT_ADDRESS, CHAIN_ID)
        setAccountOrders(userOrders);
      } else {
        setAccountOrders([]);
      }
    } catch (e) {
      toast('UNEXPECTED ERROR');
      console.log(e);
    }
  }

  const mint = async () => {
    try {
      const address = accountRef.current || await connect();
      const tx = await floozMarketplace.mintToken(address);
      const value = NFT_MINT_FEE;
      const fees = (await floozMarketplace.getProtocolFee());
      close();
      open('confirm', {
        value,
        fees,
        action: async () => {
          const res = await web3.eth.sendTransaction(tx);
          close();
          open('confirmation',
            {
              value,
              fees,
              type: 'BOUGHT',
              transactionHash: res.transactionHash
            })
          fetchAccountNFTs({invalidateCache: true});
          fetchAccountOrders();
          fetchOpenOrders();
        }})
    } catch (e) {
      toast('UNEXPECTED ERROR');
      console.log(e);
    }
  }

  const sell = async (tokenId, price) => {
    try {
      const parsedPrice = parseEther(`${price}`);

      const isApproved = await floozMarketplace.isApproved(
        NFT_ADDRESS,
        tokenId
      )
      if (!isApproved) {
        const tr = await floozMarketplace.checkApproval(
          accountRef.current,
          NFT_ADDRESS,
          tokenId
        );
        await web3.eth.sendTransaction(tr);
      }

      const order = {
        seller: accountRef.current,
        nftAddress: NFT_ADDRESS,
        tokenId: tokenId,
        price: parsedPrice,
        expiresAt: Date.now() + 3600000,
        paymentToken: WETH[ChainId.AVALANCE_FUJI_TEST],
      }
      const unsignedTx = await floozMarketplace.createOrder(order, accountRef.current);
      close();
      open('confirm', {
        value: parsedPrice,
        action: async () => {
          try {
            await web3.eth.sendTransaction(unsignedTx);
            close();
            open('confirmation',
              {
                value: parsedPrice,
                type: 'LISTED'
              })
            fetchAccountNFTs({invalidateCache: true});
            fetchAccountOrders();
            fetchOpenOrders();
          } catch (e) {
            console.log(e);
          }

        }})

    } catch (e) {
      close();
      toast('UNEXPECTED ERROR');
      console.log(e);
    }
  }

  const buy = async (order) => {
    try {
      const unsignedTx = await floozMarketplace.executeOrder(order.id, accountRef.current);
      const value = unsignedTx.value;
      const fees = (await floozMarketplace.getProtocolFee());
      close();
      open('confirm', {
        value,
        fees,
        action: async () => {
          await web3.eth.sendTransaction(unsignedTx);
          close();
          open('confirmation',
            {
              value,
              fees,
              type: 'BOUGHT'
            })
          fetchAccountNFTs({invalidateCache: true});
          fetchAccountOrders();
          fetchOpenOrders();
        }})
    } catch (e) {
      toast('UNEXPECTED ERROR');
      close();
      console.log(e);
    }
  }

  const delist = async (order) => {
    try {
      const unsignedTx = await floozMarketplace.cancelOrder(order.id);
      await web3.eth.sendTransaction(unsignedTx);
      close();
      fetchAccountNFTs({invalidateCache: true});
      fetchAccountOrders();
      fetchOpenOrders();
    } catch (e) {
      toast('UNEXPECTED ERROR');
      close();
      console.log(e);
    }
  }

  useEffect(() => {
    fetchAccountNFTs();
    fetchAccountOrders();
  }, [account])

  const contextValue = {
    connect,
    disconnect,
    account,
    network,
    fetchAccountNFTs,
    accountNFTs,
    fetchAccountOrders,
    accountOrders,
    fetchOpenOrders,
    openOrders,
    mint,
    sell,
    delist,
    buy,
  };

  return <AuthContext.Provider value={contextValue}>{children}</AuthContext.Provider>;
};

export default AuthProvider;
