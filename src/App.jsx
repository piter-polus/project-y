import 'react-spring-bottom-sheet/dist/style.css';
import 'react-toastify/dist/ReactToastify.css';

import {useContext} from "react";
import { ToastContainer, Zoom } from 'react-toastify';

import Countdown from "./screen/Countdown";
import Minting from "./screen/Minting";
import Market from "./screen/Market";
import BottomSheetLegal from "./screen/BottomSheetLegal";
import BottomSheetHelp from "./screen/BottomSheetHelp";
import BottomSheetLeaveEmail from "./screen/BottomSheetLeaveEmail";
import BottomSheetSell from "./screen/BottomSheetSell";
import BottomSheetConfirm from "./screen/BottomSheetConfirm";
import BottomSheetConfirmation from "./screen/BottomSheetConfirmation";

import styles from './App.module.scss';
import {BottomSheetsContext} from "./context/BottomSheetsProvider";
import {AuthContext, AVAILABLE_NETWORKS} from "./context/AuthProvider";
import Ball from "./components/Ball";
import NotSupportedNetwork from "./screen/NotSupportedNetwork";

const isCountdown = (new Date () < new Date('Dec 30 2021 12:00:00 PST')) && !document.URL.includes('minting') && !document.URL.includes('market')

const App = () => {
  const {open} = useContext(BottomSheetsContext);
  const {account, network, connect} = useContext(AuthContext);
  return (
    <>
      <div className={styles.app}>
        <div className={styles.wrapper}>
          <div className={styles.header}>
            <div className={styles.button}>....</div>
            {
              account ? (
                <div className={styles.button}>{account.substr(0,10)}...
                </div>
              ) : (
                isCountdown ? (<div className={styles.button} onClick={() => open('leaveEmail')}>LEAVE EMAIL</div>) : (<div className={styles.button} onClick={connect}>CONNECT</div>)
              )
            }
          </div>
          <div className={styles.content}>
            <Ball />
            <h1>
              DONDAVERSE
            </h1>
            {
              (() => {
                if (isCountdown) {
                  return (<Countdown/>)
                }

                if (account && !AVAILABLE_NETWORKS.includes(network)) {
                  return (<NotSupportedNetwork/>)
                }

                if (document.URL.includes('minting')) {
                  return (<Minting/>)
                }

                if (document.URL.includes('market')) {
                  return (<Market/>)
                }

                return (<Countdown/>)
              })()
            }
          </div>
          <div className={styles.footer}>
            <div className={styles.button} onClick={() => open('help')}>HELP</div>
            <div className={styles.button} onClick={() => open('legal')}>LEGAL</div>
          </div>
        </div>
      </div>
      <BottomSheetLegal/>
      <BottomSheetHelp/>
      <BottomSheetLeaveEmail/>
      <BottomSheetSell/>
      <BottomSheetConfirm/>
      <BottomSheetConfirmation/>
      <ToastContainer hideProgressBar={true} position="top-center" toastClassName={styles.toast} transition={Zoom} closeButton={false} />
    </>
  )
}

export default App;
