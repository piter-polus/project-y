import {detect} from 'detect-browser';

import ballGLTF from "../../assets/kanye_west_planet.gltf";
import ballUSDZ from "../../assets/kanye_west_planet.usdz";
import ballPng from "../../assets/ball.png";

const browser = detect();

const Ball = () => {
  if (browser.name === 'chrome' || browser.name === 'safari') {
    return (
      <model-viewer
        style={{ height: 250, marginBottom: 20}}
        src={ballGLTF}
        ios-src={ballUSDZ}
        poster={ballPng}
        alt="Dondaverse"
        ar
        auto-rotate
        camera-controls
      />
    )

  }

  // if (browser.name === 'safari') {
  //   return (
  //     <a rel="ar" href={ballUSDZ}>
  //       <video width="400" height="400" autoPlay loop muted playsInline>
  //         <source
  //           src={ballMov}
  //           type='video/mp4; codecs="hvc1"' />
  //         <source src={ballWebm} type="video/webm"/>
  //         <img src={ballPng} alt="Dondaverse" />
  //       </video>
  //     </a>
  //   )
  // }

  return (
    <img src={ballPng} alt="Dondaverse" />
  )

}

export default Ball

