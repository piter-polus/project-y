
import {useState} from "react";

import styles from './Tooltip.module.scss'

const Tooltip = ({text, children}) => {
  const [isActive, setIsActive] = useState(false);


  return (
    <div className={styles.container} onMouseEnter={() => setIsActive(true)} onMouseLeave={() => setIsActive(false)}>
      {
        children
      }
      {
        isActive && (
          <div className={styles.tooltip}>
            {
              text
            }
          </div>
        )
      }
    </div>
  )
}

export default Tooltip

