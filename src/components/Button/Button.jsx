import cx from "classnames";
import styles from "./Button.module.scss";
import {useCallback, useState} from "react";
import LoadingDots from "../LoadingDots";

const Button = ({text, loadingText = "LOADING", onClick, disabled}) => {
  const [isLoading, setIsLoading] = useState(false);

  const action = useCallback(async () => {
    try {
      setIsLoading(true);
      await onClick();
      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
    }
  }, [onClick])

  return (
    <div className={cx([styles.button, disabled && styles.disabled])} onClick={(!disabled && !isLoading) ? action : null}>
      {
        isLoading ? (<div className={styles.text}>{loadingText}<LoadingDots />
        </div> ) : (<div className={styles.text}>
          {text}
        </div>)
      }
    </div>
  )
}

export default Button

