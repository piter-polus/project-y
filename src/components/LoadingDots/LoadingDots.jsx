
import {useEffect, useState} from "react";


const LoadingDots = () => {
  const [dots, setDots] = useState([]);

  useEffect(() => {
    const interval = setInterval(() => {
      setDots(dots => {
        if (dots.length >=3) {
          return [];
        } else {
          return [...dots, '.']
        }
      })
    }, 500)

    return () => {
      clearInterval(interval)
    }
  }, )

  return (
    <span>
      {
        dots.join('')
      }
    </span>
  )
}

export default LoadingDots

