import {BottomSheet} from 'react-spring-bottom-sheet';
import {useContext} from "react";

import {BottomSheetsContext} from "../../context/BottomSheetsProvider";
import Button from '../../components/Button'

import styles from "./BottomSheetHelp.module.scss";
import {fetchMoonpaySignature, MOONPAY_API_KEY} from "../../utils/api";
import {AuthContext} from "../../context/AuthProvider";

const BottomSheetHelp = () => {
  const {activeBottomSheet, close} = useContext(BottomSheetsContext);
  const {account} = useContext(AuthContext);
  return (
    <BottomSheet
      open={activeBottomSheet === 'help'}
      onDismiss={close}
    >
      <div className={styles.wrapper}>
        <h2>
          HOW TO
        </h2>
        <p>
          NFTs, or non-fungible tokens, are blockchain-based tokens that prove ownership of digital items such as
          images, video files or (less commonly) physical assets.
        </p>
        <p>
          In this guide, we’ll walk you through a few simple steps to get started with buying The Ye and Drake — Larry
          Hoover NFT:
        </p>
        <ul>
          <li>Buy Avalanche ($AVAX) on a cryptocurrency exchange like Coinbase or Kraken.</li>
          <li>Get a crypto wallet like Metamask.</li>
          <li>Send XXX from your cryptocurrency exchange to your wallet.</li>
          <li> Sync your wallet with DONDA.LIVE</li>
          <li>Buy the NFT on DONDA.LIVE with $AVAX stored on your wallet.</li>
        </ul>
        <hr/>
        <h2>
          BUY $AVAX
        </h2>
        <p>
          To purchase this NFT you need $AVAX.
          To get $AVAX, you’ll need to head to a cryptocurrency exchange, such as Coinbase or Binance.
        </p>
        <p>
          Not all exchanges let you buy and sell all cryptocurrencies. $AVAX is listed on almost all exchanges. And if
          you’re absolutely new to crypto and have no idea which exchange to use, here’s a guide to help you make a
          decision.
        </p>
        <div style={{ marginBottom: 20}}>
          <Button  onClick={async () => {
            const {signature} = await fetchMoonpaySignature(account);
            window.open(`https://buy-staging.moonpay.com?apiKey=${MOONPAY_API_KEY}&currencyCode=matic&walletAddress=${account}&signature=${signature}`, '_blank');
          }} text="BUY $AVAX WITH MOONPAY" />
        </div>
        <Button onClick={() => {
          window.open(`https://buy.ramp.network/?userAddress=${account}`, '_blank');
        }} text="BUY $AVAX WITH RAMP" />
        <p>
          Onto the second stage: getting a crypto wallet
        </p>
        <hr/>
        <h2>
          Get a crypto wallet
        </h2>
        <p>
          There are many wallets out there. But by far the most popular is MetaMask.
        </p>
        <p>
          MetaMask is a browser plugin, and it works best with Google Chrome or Brave Browser. Once it's installed, it
          lets you store $AVAX and other tokens.
        </p>
        <p>
          Setting up a crypto wallet may sound daunting, but it’s often quick and seamless. Download and install
          MetaMask through their website, and once the little fox logo appears on your browser, click on it. It will
          take you through a few quick steps. Most importantly, you’ll create a password.
        </p>
        <p>
          Separately, MetaMask will assign you a “Secret Recovery Phrase,” which you'll need to store somewhere safe,
          like on a piece of paper in a secure spot. It’s a twelve-word phrase that can be used to unlock a MetaMask
          wallet.
        </p>
        <p>
          Take extra care with your crypto wallet. Phishing scams targeting MetaMask users are common on social media.
          Sometimes they can be so realistic that even seasoned NFT collectors have fallen for them. But no need to
          panic: you’ll be safe as long as you don’t share your seed phrase.
        </p>
        <hr/>
        <h2>
          SEND AVAX TO YOUR WALLET
        </h2>
        <p>
          Now that you have $AVAX (step 1) and MetaMask (step 2), it’s time to fund your wallet with $AVAX.
        </p>
        <p>
          The way NFT trade works is a bit like going to a farmer’s market that doesn't take cards, so you’ll want to
          carry cash in your wallet.
        </p>
        <p>
          Go to your exchange’s “send” or “withdraw” page that lets you move funds to a crypto wallet. It will ask you
          to enter the amount you want to send and a blockchain address, and so you will need to copy your Avalanche
          $AVAX public address as displayed on your MetaMask (starts with 0x). Think of your $AVAX address (or “addy” in
          crypto slang) as your bank account number on the blockchain, sort of.
        </p>
        <p>
          Remember, MetaMask automatically generates an Avalanche $AVAX public address for you when you set up the
          wallet. In the future, you can separately create as many addresses as you want.
        </p>
        <hr/>
        <h2>
          SYNCH YOUR WALLET WITH DONDA.LIVE
        </h2>
        <p>
          This NFT is traded on DONDA.LIVE.
        </p>
        <p>
          Signing up for DONDA.LIVE takes a few clicks. At the top-right corner of the DONDA.LIVE website, you’ll see a
          wallet icon. Click on it, and it will show you a long list of supported crypto wallets. Choose MetaMask, if
          you’ve followed the earlier step and got yourself MetaMask. If you've opted for an alternative, select the
          appropriate wallet from the list; many wallets support the WalletConnect protocol for connecting to websites.
        </p>
        <p>
          Once you choose MetaMask from that list, DONDA.LIVE will show a pop-up window that says “Connect with
          MetaMask.” Click next, and your profile will automatically be created. And that’s done!
        </p>
        <hr/>
        <h2>
          BUY AN NFT
        </h2>
        <p>
          Crypto industry isn’t known for user-friendly platforms and apps. But DONDA.LIVE is an exception: the site is
          mostly intuitive and feels like browsing any other e-commerce platform.
        </p>
        <hr/>
        <h3>
          BUY NOW
        </h3>
        <p>
          After you decide to buy a NFT in the primary or in the secondary market, you will have three options: buy now,
          make offer, and place bid.
        </p>
        <p>
          NFTs may be listed by their owners at a fixed price (“current price”). You can buy that NFT by paying the
          asking price. For that, you’ll need to click “buy now” and follow the steps. Make sure to have enough $AVAX on
          MetaMask before proceeding!
          If you want to buy an NFT at a fixed price, you’ll need to pay transaction fees—known as “gas” in $AVAX—that
          are anything but fixed. The price frequently fluctuates, which can be confusing to newcomers.

        </p>
        <p>
          You can check the state of gas here—an NFT transaction would incur gas similar to a ”ERC20 Transfer” so you
          can use that one as a rough estimate. MetaMask will suggest an amount depending on the network conditions at
          the time, so you don’t have to calculate it yourself.

        </p>
        <hr/>
        <h3>
          Make offer
        </h3>
        <p>
          But perhaps you fancy haggling and think your counter-offer will be enticing. If so, you can make an offer and
          see whether the seller accepts it or not.
          And if you make an offer on an NFT and the seller accepts your offer, then the seller pays the gas.

        </p>
        <hr/>
        <h3>
          Place bid
        </h3>
        <p>
          Some sellers like a bit of competition for their NFTs so they’ll put them up for auction instead of selling
          them at a fixed price. Your only option is to place a bid at a price that you think is fair.

        </p>
        <p>
          But your bid must be at least 5% higher than the previous bid. The highest bidder will win the auction, if it
          also meets the minimum bid requirement.
        </p>
        <p>
          And finally, there are some NFT owners who don’t list their NFTs for sale—either at a fixed price or for
          auction. But that doesn’t mean you can’t try and lure them with an appealing offer. Those NFTs will only have
          a “make offer” option available. If you don't ask, you don't get.

        </p>
        <p>
          If you’ve followed the steps so far and have made a purchase already, then congratulations on acquiring your
          first ever NFT! Click on your profile at the top-right corner, and you’ll see that your NFT purchases are
          listed there.
        </p>
      </div>
    </BottomSheet>
  );
}

export default BottomSheetHelp;
