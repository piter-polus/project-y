import {BottomSheet} from 'react-spring-bottom-sheet';
import {useContext} from "react";

import {BottomSheetsContext} from "../../context/BottomSheetsProvider";

import styles from "./BottomSheetConfirmation.module.scss";
import Button from "../../components/Button";
import {FormatAvax} from "../../utils/formatters";

const BottomSheetConfirmation = () => {
  const {activeBottomSheet, activeBottomSheetData: {title, value, type, transactionHash}, close} = useContext(BottomSheetsContext);
  return (
    <BottomSheet
      open={activeBottomSheet === 'confirmation'}
      onDismiss={close}
    >
      <div className={styles.wrapper}>
        <div className={styles.details}>
          {
            title && (
              <div className={styles.row}>
                TRANSACTION VERIFIED
              </div>
            )
          }
          {
            value && (
              <div className={styles.row}>
                {type || 'BOUGHT'} FOR <FormatAvax value={value} />
              </div>
            )
          }
          {
            transactionHash && (
              <div className={styles.row}>
                <a className={styles.link} href={`https://snowtrace.io/tx/${transactionHash}`} target="_blank">TX RECEIPT</a>
              </div>
            )
          }
        </div>
        <Button text="CONTINUE" onClick={close} />
      </div>
    </BottomSheet>
  );
}

export default BottomSheetConfirmation;
