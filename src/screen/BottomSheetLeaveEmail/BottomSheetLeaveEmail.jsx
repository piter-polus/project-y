import {BottomSheet} from 'react-spring-bottom-sheet';
import {useContext, useEffect, useState} from "react";
import {toast} from "react-toastify";

import {BottomSheetsContext} from "../../context/BottomSheetsProvider";
import fetch from "../../utils/fetch"

import styles from "./BottomSheetLeaveEmail.module.scss";
import Button from "../../components/Button";


const BottomSheetLeaveEmail = () => {
  const {activeBottomSheet, close} = useContext(BottomSheetsContext);
  const [email, setEmail] = useState('');
  const [isEmailValid, setIsEmailValid] = useState(false);

  useEffect(() => {
    setIsEmailValid(/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(email));
  }, [email, setIsEmailValid])

  const save = async () => {
    if (!isEmailValid) return;
    await fetch.post('https://us-central1-project-y-17652.cloudfunctions.net/subscribeToMailingList', {
      email
    });
    toast("Thanks");
    close();
  }

  return (
    <BottomSheet
      open={activeBottomSheet === 'leaveEmail'}
      onDismiss={close}
    >
      <div className={styles.wrapper}>
        <input className={styles.input} type="email" value={email} onChange={(event) => {
          setEmail(event.target.value)
        }} />
        <Button text={`LEAVE EMAIL`} disabled={!isEmailValid} onClick={save} />
      </div>
    </BottomSheet>
  );
}

export default BottomSheetLeaveEmail;
