import {BottomSheet} from 'react-spring-bottom-sheet';
import {useContext} from "react";

import {BottomSheetsContext} from "../../context/BottomSheetsProvider";

import styles from "./BottomSheetLegal.module.scss";

const BottomSheetLegal = () => {
  const {activeBottomSheet, close} = useContext(BottomSheetsContext);
  return (
    <BottomSheet
      open={activeBottomSheet === 'legal'}
      onDismiss={close}
    >
      <div className={styles.wrapper}>

      </div>
    </BottomSheet>
  );
}

export default BottomSheetLegal;
