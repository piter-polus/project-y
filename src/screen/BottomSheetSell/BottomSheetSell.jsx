import {BottomSheet} from 'react-spring-bottom-sheet';
import {useContext, useEffect, useState} from "react";
import {parseEther} from "@ethersproject/units";

import {BottomSheetsContext} from "../../context/BottomSheetsProvider";

import styles from "./BottomSheetSell.module.scss";
import {AuthContext} from "../../context/AuthProvider";
import Button from "../../components/Button";
import {FormatAvax} from "../../utils/formatters";

const BottomSheetSell = () => {
  const {activeBottomSheet, activeBottomSheetData: nft, close} = useContext(BottomSheetsContext);
  const { sell } = useContext(AuthContext);
  const [price, setPrice] = useState(0.1);

  const [priceInWei, setPriceInWei] =  useState(null);

  useEffect(() => {
    try {
      const priceInWei = parseEther(`${price}`);
      setPriceInWei(priceInWei);
    } catch (e) {
      setPrice(null)
    }
  }, [price])

  const action = async () => {
    await sell(nft.tokenId, price);
  }

  return (
    <BottomSheet
      open={activeBottomSheet === 'sell'}
      onDismiss={close}
    >
      <div className={styles.wrapper}>
        <input placeholder="Enter price in AVAX" className={styles.input} type="number" value={price} onChange={(event) => {
          setPrice(event.target.value)
        }} />
        <Button text={
          <>
            <div className={styles.buttonLabel}>LIST FOR SALE</div>({priceInWei && (<FormatAvax value={priceInWei}/>)})
          </>
        } onClick={action} loadingText="LISTING FOR SALE" disabled={!priceInWei} />
      </div>
    </BottomSheet>
  );
}

export default BottomSheetSell;
