import {useContext} from "react";

import {AuthContext} from "../../context/AuthProvider";
import {BottomSheetsContext} from "../../context/BottomSheetsProvider";
import {FormatAvax} from "../../utils/formatters";

import styles from './Market.module.scss';

const Market = () => {
  const {account, accountNFTs, connect, delist, buy, openOrders, fetchAccountNFTs, fetchOpenOrders, accountOrders, fetchAccountOrders} = useContext(AuthContext);
  const {open} = useContext(BottomSheetsContext);

  return (account ? (
    <>
      <div className={styles.offersHeader}>
        <div>
          MY NFTS
        </div>
        <div className={styles.button} onClick={fetchAccountNFTs}>
          REFRESH
        </div>
      </div>
      <div className={styles.offers}>
        {accountNFTs.map(nft => (
          <div className={styles.offer} key={nft.tokenId}>
            <div className={styles.id}>
              ID:{nft.tokenId}
            </div>
            <div className={styles.button} onClick={() => open('sell', nft)}>
              LIST FOR SALE
            </div>
          </div>
        ))}
      </div>
      <div className={styles.offersHeader}>
        <div>
          MY LISTED FOR SALE
        </div>
        <div className={styles.button} onClick={fetchAccountOrders}>
          REFRESH
        </div>
      </div>
      <div className={styles.offers}>
        {accountOrders.map(offer => (
          <div className={styles.offer} key={offer.id}>
            <div className={styles.id}>
              ID:{offer.assetId}
            </div>
            <div className={styles.price}>
              <FormatAvax value={offer.priceInWei} />
            </div>
            <div className={styles.button} onClick={() => {delist(offer);}}>
              DELIST
            </div>
          </div>
        ))}
      </div>
      <div className={styles.offersHeader}>
        <div>
          SECONDARY MARKET
        </div>
        <div className={styles.button} onClick={fetchOpenOrders}>
          REFRESH
        </div>
      </div>
      <div className={styles.offers}>
        {openOrders.map(o => (
          <div className={styles.offer} key={o.id}>
            <div className={styles.price}>
              <FormatAvax value={o.priceInWei} />
            </div>
            <div className={styles.sellers}>
              {o.seller.substr(0,8)}...
            </div>
            <div className={styles.button} onClick={() => buy(o)}>
              BUY
            </div>
          </div>
        ))}
      </div>
    </>
  ) : (
    <div className={styles.button} onClick={ connect } >
      CONNECT
    </div>
  ));
}

export default Market;
