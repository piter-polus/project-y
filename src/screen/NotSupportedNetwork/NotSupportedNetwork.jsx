import styles from './NotSupportedNetwork.module.scss';

const NotSupportedNetwork = () => {
  return (
    <div className={styles.countdown}>
      <h1>
        NOT SUPPORTED NETWORK
      </h1>
      <h1>
        SWITCH TO AVALANCHE NETWORK TO USE DONDAVERSE
      </h1>
    </div>
  );
}

export default NotSupportedNetwork;
