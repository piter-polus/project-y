import {useContext} from "react";

import styles from './Minting.module.scss';
import {AuthContext} from "../../context/AuthProvider";
import Tooltip from "../../components/Tooltip";

const Minting = () => {
  const {accountNFTs, mint} = useContext(AuthContext);
  return (accountNFTs.length ? (
      <>
        <h1>
          BOUGHT FOR $100
        </h1>
        <Tooltip text="AVAILABLE ONCE ALL NFTS ARE MINTED">
          <div className={styles.button}>
            LIST FOR SALE
          </div>
        </Tooltip>
      </>
    ) : (
      <>
        <h1>
          DONDAVERSE
        </h1>
        <div className={styles.button} onClick={ mint } >
          BUY
        </div>
      </>
    )
  );
}

export default Minting;
