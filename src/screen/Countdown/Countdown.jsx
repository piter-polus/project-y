import ReactCountdown from 'react-countdown';

import styles from './Countdown.module.scss';

const Countdown = () => {
  return (
    <div className={styles.countdown}>
      <ReactCountdown
        date={new Date('Dec 30 2021 12:00:00 PST')}
      />
    </div>
  );
}

export default Countdown;
