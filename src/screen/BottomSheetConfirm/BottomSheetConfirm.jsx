import {BottomSheet} from 'react-spring-bottom-sheet';
import {useContext} from "react";
import cx from "classnames";

import {BottomSheetsContext} from "../../context/BottomSheetsProvider";

import styles from "./BottomSheetConfirm.module.scss";
import Button from "../../components/Button";
import Tooltip from "../../components/Tooltip"
import {FormatAvax} from "../../utils/formatters";

const BottomSheetConfirm = () => {
  const {activeBottomSheet, activeBottomSheetData: {value, fees, action}, close} = useContext(BottomSheetsContext);

  const confirm = async () => {
    await action();
  }

  return (
    <BottomSheet
      open={activeBottomSheet === 'confirm'}
      onDismiss={close}
    >
      <div className={styles.wrapper}>
        <div className={styles.details}>
          {
            value && (
              <div className={cx([styles.row, styles.opacity])}>
                <div className={styles.left}>
                  PRICE
                </div>
                <div className={styles.right}>
                  {value && <FormatAvax value={value} />}
                </div>
              </div>
            )
          }
          {
            fees && (
              <div className={cx([styles.row, styles.opacity])}>
                <div className={styles.left}>
                  FEES
                  <Tooltip text="INCLUDES PROCESSING, EXCLUDES GAS PRICES">
                    <div className={styles.questionmark}>?</div>
                  </Tooltip>
                </div>
                <div className={styles.right}>
                  <FormatAvax value={fees} />
                </div>
              </div>
            )
          }
          {
            (fees && value) && (
              <div className={styles.row}>
                <div className={styles.left}>
                  TOTAL
                </div>
                <div className={styles.right}>
                  <FormatAvax value={value.add(fees)} />
                </div>
              </div>
            )
          }
        </div>
        <Button text="CONFIRM" onClick={confirm} loadingText="CONFIRMING" />
      </div>
    </BottomSheet>
  );
}

export default BottomSheetConfirm;
